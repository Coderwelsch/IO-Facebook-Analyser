# IO-Facebook-Analyser
![Mockup](https://raw.githubusercontent.com/Coderwelsch/IO-Facebook-Analyser/master/assets/mockup.png)
The Facebook-Analyser script is a small script for analysing your profile posts and displays relations and interactions of you and your friends.
If it is installed and activated this script will inject a small element in your facebook profile view.

##Requirements & Instructions
1. Use your Chrome / Firefox browser and install [Greasemonkey](https://addons.mozilla.org/de/firefox/addon/greasemonkey/) or [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=de)
2. Create a new usescript in that browser plugin and copy / paste the script content of _web/chronic-pluginjs_. 
3. Go to your facebook profile site and enjoy the new element in your timeline.
4. Replace your username in the script with your username under *// @match        https://www.facebook.com/YOURUSERNAME*

Screenshot / Example:
![Working Example](https://raw.githubusercontent.com/Coderwelsch/IO-Facebook-Analyser/master/assets/screenshot.png)

##Processing Scripts
I wrote several scripts in processing to find a good start for my Facebook Analyser tool. Here these scripts:

###Mood-Analyser
![Mood-Analyser](https://raw.githubusercontent.com/Coderwelsch/IO-Facebook-Analyser/master/assets/mood_analyser.png)
I tested some functionalities and animations in processing to visualise the analyser tool. By the way - it looks like a paint of first-graders :-D

To test the script, please install the YahooWeather library from https://github.com/onformative/YahooWeather under your Processing/library folder.

###Facebook-Analyser | First Test
![Facebook-Analyser](https://raw.githubusercontent.com/Coderwelsch/IO-Facebook-Analyser/master/assets/facebook_analyser.png)
This Processing script was the second try to implement the Facebook-Analyser-Tool. But regarding to the 
capabilities of Processing and the integration to the Facebook website - i was forced to use javascript and userscripting to archieve my purposes.
