// ==UserScript==
// @require      http://code.jquery.com/jquery-2.1.3.min.js
// @resource     mainCSS https://www.coderwelsch.com/files/projects/eingabe-ausgabe/facebook-analyser/main.css
// @grant        GM_getResourceText
// @grant        GM_addStyle
// @name         Facebook Chronic Plugin
// @namespace    http://your.homepage/
// @version      0.1
// @description  enter something useful
// @author       You
// @match        https://www.facebook.com/Hurryflame
// @grant        none
// ==/UserScript==          

var mainCSS = GM_getResourceText( "mainCSS" );
GM_addStyle( mainCSS );

// global variables
var $timeline,
    $chronicContainer,
    $startButton,
    $chronicPosts,
    $ownHead,
    ownProfileImgUrl,
    userData;


// proto functions
function initPrototypes () {
    Object.prototype.length = function () {
        var l = 0;

        for ( var k in this ) {
            if ( this.hasOwnProperty( k ) ) {
                l++;
            }
        }

        return l;
    }
}


// main functions
function initVars () {
    $timeline = $( '#pagelet_main_column_personal' );
    $chronicContainer = $( '<div class="chronic-plugin"></div>' ).prependTo( $timeline );
    $startButton = $( '<div class="start-button"><p>Plugin starten</p></div>' ).appendTo( $chronicContainer );
    $chronicPosts = $( '.userContentWrapper' );
    ownProfileImgUrl = $( '.profilePicThumb>.profilePic' ).attr( 'src' );
    userData = [];
}

function bindEvents () {

}

function generateOwnChatHead () {
    $ownHead = $( '<div class="chat-head main-head"></div>' )
        .css( 'background-image', 'url(' + ownProfileImgUrl + ')' )
        .appendTo( $chronicContainer );
}

function analyzeChronic () {
    $chronicPosts.each( function () {
        var $post = $( this ).clone(),
            $userContent = $post.find( '.userContent' ),
            $likes = $post.find( '.profileLink' ),
            $images = $post.find( '.scaledImageFitWidth' ),
            htmlText = $userContent.html();

        userData.push( {
            text: htmlText,
            likes: $likes,
            images: $images
        } );
    } );
}

function displayAnalyzedData () {
    for ( var i = 0; i < userData.length; i++ ) {
        var item = userData[ i ],
            $post = $( '<div class="chronic-post"><div class="post-img"></div><div class="post-text"></div></div>' ),
            $text = $post.find( '.post-text' ),
            $img = $post.find( '.post-img' );

        if ( item.images.length ) {
            $img.css( 'background-image', 'url(' + item.images.first()[ 0 ].src + ')' );
        }

        if ( item.likes.length ) {
            var $likes = $( '<div class="likes"></div>' );

            for ( var i = 0; i < item.likes.length; i++ ) {
                var like = item.likes[ i ];

                $likes.append( like );
            }

            $text.append( $likes );
        }

        $chronicContainer.append( $post );

        // analyze maximum
        if ( i > 10 ) {
            break;
        }
    }
}

// init functions
function main () {
    generateOwnChatHead();
    analyzeChronic(); 
    displayAnalyzedData();
}

function init () {
    initPrototypes();
    initVars();
    bindEvents();

    $startButton.on( 'click', function () {
        $( this ).hide();

        main();
    } );
}

$( document ).ready( init );