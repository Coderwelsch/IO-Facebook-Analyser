# imports
import re

# global variables
#   sets the width an height of the stage
stageWidth = 850
stageHeight = 500

#   regex to find friend names in lines of the .txt file
regexFriends = "mit:(\D+)"

#   my own "chathead" settings
me = { "name": "Joseph Ribbe", "root": True, "text-size": 15, "img": "joseph-ribbe.png", "pos-x": stageWidth / 2 - 150 / 2, "pos-y": 20, "width": 150, "height": 150 }

#   "chathead" settings of my friends
fbFriends = [ { "name": "Katrin Ribbe", "img": "katrin-ribbe.png", "pos-x": stageWidth *0.2 - 150 / 2, "pos-y": stageHeight * 0.6, "width": 100, "height": 100 },
              { "name": "Angela Merkel", "img": "angela-merkel.png", "pos-x": stageWidth * 0.8 - 150 / 2, "pos-y": stageHeight * 0.6, "width": 100, "height": 100 },
              { "name": "Tobias Polzin", "img": "tobias-polzin.png", "pos-x": stageWidth / 2 - 150 / 2, "pos-y": stageHeight * 0.7, "width": 100, "height": 100 } ]

#   variables for text analysis
fbTxtFile = ""
fbTxt = []
fbActivity = {}

# functions
#   analyses the loaded .txt file and create for each friend a "chathead"
def analyzeActivities():
    global fbTxt, regexFriends, fbFriends
    
    for item in fbTxt:
        r = re.search( regexFriends, item )
        
        if item[ 0 ] == "#":
            continue
        
        if ( r ):
            string = r.group( 1 ).replace( '\n', '' )
            
            for item in fbFriends:
                if item[ "name" ] == string:
                    createChatHead( item )


#   creates "chatheads"    
def createChatHead ( data ):
    if not "root" in data:
        stroke( 4 )
        line( int( data[ "pos-x" ] ) + int( data[ "width" ] ) / 2, 
              int( data[ "pos-y" ] ) + int( data[ "height" ] ) / 2, 
              int( me[ "pos-x" ] ) + int( me[ "width" ] ) / 2, 
              int( me[ "pos-y" ] ) + int( me[ "height" ] ) / 2)
    
    img = loadImage( "imgs/" + data[ "img" ] )
    image( img, int( data[ "pos-x" ] ), int( data[ "pos-y" ] ), int( data[ "width" ] ), int( data[ "height" ] ) )
    
    fill( 100, 100, 100 )
    
    # textSize( "text-size" in data ? data[ "text-size" ] : 12 )
    if "text-size" in data:
       textSize( int( data[ "text-size" ] ) )
    else:
        textSize( 12 )
    
    textAlign( CENTER )
    text( data[ "name" ], int( data[ "pos-x" ] ) + int( data[ "width" ] ) / 2, int( data[ "pos-y" ] + int( data[ "height" ] ) + 10 ) )

#   setup the stage, draw my "chathead", same for the other users                                    
def setup():
    global me
    
    # draw white bg
    size(stageWidth, stageHeight)
    background(255)

    # load fb chronic posts
    with open( 'txt/facebook-chronic.txt', 'r' ) as file:
        global fbTxt 
        fbTxt = file.readlines()

    # analyze activities
    analyzeActivities()
    
    # creates my own chat head
    createChatHead( me )
