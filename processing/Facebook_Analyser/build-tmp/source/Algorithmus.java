import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import com.onformative.yahooweather.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Algorithmus extends PApplet {

/*import java.util.regex.*;

int stageWidth = 850;
int stageHeight = 300;

String[] fbText;
ArrayList<String> activities;

int moodValue; // 0 - 10

String regexGoodMood = "( - (hoffnungsvoll|fantastisch|fabelhaft|stark|belustigt))|(- schaut:(Mr. Bean|Interstellar))";
String regexActivity = " - (schaut|liest|hoert|lernt|streitet):";
String regexFriends = "mit:(.)";
String regexBadMood = "(- (schmerzerfuellt|traurig|wuetend|hass|armselig))|( - schaut:(Beim Leben meiner Schwester|Titanic))";


public void setup () {
	size ( stageWidth, stageHeight );
	background ( 255 );

	// load txt file
	fbText = loadStrings( "txt/facebook-chronic.txt" );

	//mood
	moodValue = getMoodValue( fbText );

	println( moodValue );
}

public void draw () {

}

public  int getMoodValue ( String[] txt ) {
	int m = 5;

	for ( int i = 0; i < txt.length; i++ ) {
		String row = txt[ i ];
		Boolean isBadMood = Pattern.matches( row, regexBadMood );
		Boolean isGoodMood = Pattern.matches( row, regexGoodMood );

		println(Pattern.matches( "(a-zA-Z)", "HELLO" ));


		if ( isBadMood ) {
			m--;
		} else if ( isGoodMood ) {
			m++;
		} else {
			// do nothing
		}
	}

	return m;
}*/

/*public void setup () {
	size ( stageWidth, stageHeight );
	background ( 255 );

	// load txt file
	fbText = loadStrings( "txt/facebook-chronic.txt" );

	// get mood
	moodValue = getMoodByText ( fbText );
	// get activites
	activities = getActivitiesByText ( fbText );

	println( moodValue );
	println( activities );
}

public void draw () {

}

public int getMoodByText ( String[] txt ) {
	int m = 0; // 0 - 10

	// bad moods
	for ( int i = 0; i < txt.length; i++ ) {
		Boolean found = regexBadMood.matcher( txt [ i ] ).matches();
		Matcher regexFriendsMatcher = regexFriends.matcher( txt[ i ] );

		println( found );		

		if ( found != null ) {
			m--;
		}

		if ( regexFriendsMatcher != null ) {
			// println( regexFriendsMatcher );
		}
	}

	// good mood
	for ( int i = 0; i < txt.length; i++ ) {
		Matcher regexGoodMoodMatcher = regexGoodMood.matcher( txt [ i ] );
		Matcher regexFriendsMatcher = regexFriends.matcher( txt[ i ] );

		if ( regexGoodMoodMatcher != null ) {
			m++;
		}

		if ( regexFriendsMatcher != null ) {
			// println( regexFriendsMatcher );
		}
	}

	return m;
}

public ArrayList<String> getActivitiesByText ( String[] txt ) {
	ArrayList<String> a = new ArrayList<String>();
	
	for ( int i = 0; i < txt.length; i++ ) {
		Matcher regexActivityMatcher = regexActivity.matcher( txt [ i ] );

		if ( regexActivityMatcher != null ) {
			//a.add( regexActivityMatcher [ 1 ] );
		}
	}

	return a;
}*/




String[] svgsSunny = { 
	"svg/sun_01.svg", 
	"svg/sun_02.svg", 
	"svg/sun_03.svg"
};

String[] svgsInsects = {
	"svg/insect_01.svg",
	"svg/insect_02.svg",
	"svg/insect_03.svg"
};

String[] svgsCold = {
	"svg/windy_01.svg",
};

String[] svgsNight = {
	"svg/night-moon.svg"
};

String[] svgsDay = {
	"svg/flower_01.svg",
	"svg/flower_02.svg",
	"svg/grass_01.svg"
};

Boolean dragging = false;

YahooWeather weather;
FormShape[] shapes = new FormShape[ 10 ];

public void setup () {
	size( 850, 500 );

	weather = new YahooWeather( this, 638242, "c", 3000 );
	weather.update();

	initShapes();
}

public void draw () {
	weather.update();

	fill( 255 );
	rect( 0, 0, width, height );

	redrawShapes();
	draggingMouse();
}

public void mousePressed () {
	println(11);

	dragging = true;
}

public void mouseReleased () {
	dragging = false;

	println(33);
}

public void draggingMouse () {
	if ( dragging ) {
		for ( int i = 0; i < 10; i++ ) {
			if ( shapes[ i ].x > mouseX && shapes[ i ].x + shapes[ i ].formWidth < mouseX && shapes[ i ].y > mouseY && shapes[ i ].y + shapes[ i ].formHeight < mouseY ) {
				println( true );
			}
		}
	}
}

public void initShapes () {
	for ( int i = 0; i < 2; i++ ) {
		if ( weather.getWeatherCondition() == "Partly Cloudy" ) {
			shapes[ i ] = new FormShape( svgsCold[ (int) random( svgsCold.length ) ] );
		} else {
			shapes[ i ] = new FormShape( svgsSunny[ (int) random( svgsSunny.length ) ] );
		}
	}

	for ( int i = 2; i < 6; i++ ) {
		if ( hour() > 12 && hour() < 18 ) {
			shapes[ i ] = new FormShape( svgsDay[ (int) random( svgsDay.length ) ] );
		} else {
			shapes[ i ] = new FormShape( svgsNight[ (int) random( svgsNight.length ) ] );
		}
	}

	// fill up the rest
	for ( int i = 6; i < 10; i++ ) {
		shapes[ i ] = new FormShape( svgsSunny[ (int) random( svgsSunny.length ) ] );
	}
}

public void redrawShapes () {
	// create lines
	for ( int i = 0; i < 10; i++ ) {
		if ( i < 9 ) {
			stroke(0);
			strokeWeight(5);
			drawDottedLine( shapes[ i ].x, shapes[ i ].y, shapes[ i + 1 ].x, shapes[ i + 1 ].y );
		}	
	}

	// redraw shapes
	for ( int i = 0; i < 10; i++ ) {
		shapes[ i ].drawShape();
	}
}

public void drawDottedLine ( int x1, int y1, int x2, int y2 ) {
	for ( int i = 0; i < 15; i++ ) {
		float x = lerp( x1, x2, i / 15.0f );
		float y = lerp( y1, y2, i / 15.0f );
		point( x, y );
	}
}

class FormShape {
	public int formWidth;
	public int formHeight;
	public float rotation;
	public int x;
	public int y;
	public int svgIndex;
	public float rotationSin;
	public float rotationIntensity;

	public PShape formShape;

	FormShape ( String svgPath ) {
		formWidth = (int) random( 100, 200 );
		formHeight = formWidth;
		rotationSin = 0;
		rotationIntensity = random( -0.03f, 0.03f );
		x = (int) random( formWidth / 2, width - formWidth / 2 );
		y = (int) random( formHeight / 2, height - formHeight / 2 );

		if ( weather.getWindTemperature() > 13 ) {
			formShape = loadShape( svgPath );
		} else {
			formShape = loadShape( svgPath );
		}
	}	

	public void drawShape () {
		rotationSin += rotationIntensity;
		rotation = 0.2f * sin( rotationSin );

		pushMatrix();
		translate( x, y );
		rotate( rotation );
		translate( -formWidth / 2, -formHeight / 2 );
		shape( formShape, 0, 0, formWidth, formHeight );
		popMatrix();
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Algorithmus" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
