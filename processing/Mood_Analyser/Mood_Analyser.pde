import com.onformative.yahooweather.*;

String[] svgsSunny = { 
	"svg/sun_01.svg", 
	"svg/sun_02.svg", 
	"svg/sun_03.svg"
};

String[] svgsInsects = {
	"svg/insect_01.svg",
	"svg/insect_02.svg",
	"svg/insect_03.svg"
};

String[] svgsCold = {
	"svg/windy_01.svg",
};

String[] svgsNight = {
	"svg/night-moon.svg"
};

String[] svgsDay = {
	"svg/flower_01.svg",
	"svg/flower_02.svg",
	"svg/grass_01.svg"
};

Boolean dragging = false;

YahooWeather weather;
FormShape[] shapes = new FormShape[ 10 ];

void setup () {
	size( 850, 500 );

	weather = new YahooWeather( this, 638242, "c", 3000 );
	weather.update();

	initShapes();
}

void draw () {
	weather.update();

	fill( 255 );
	rect( 0, 0, width, height );

	redrawShapes();
	draggingMouse();
}

void mousePressed () {
	println(11);

	dragging = true;
}

void mouseReleased () {
	dragging = false;

	println(33);
}

void draggingMouse () {
	if ( dragging ) {
		for ( int i = 0; i < 10; i++ ) {
			if ( shapes[ i ].x > mouseX && shapes[ i ].x + shapes[ i ].formWidth < mouseX && shapes[ i ].y > mouseY && shapes[ i ].y + shapes[ i ].formHeight < mouseY ) {
				println( true );
			}
		}
	}
}

void initShapes () {
	for ( int i = 0; i < 2; i++ ) {
		if ( weather.getWeatherCondition() == "Partly Cloudy" ) {
			shapes[ i ] = new FormShape( svgsCold[ (int) random( svgsCold.length ) ] );
		} else {
			shapes[ i ] = new FormShape( svgsSunny[ (int) random( svgsSunny.length ) ] );
		}
	}

	for ( int i = 2; i < 6; i++ ) {
		if ( hour() > 12 && hour() < 18 ) {
			shapes[ i ] = new FormShape( svgsDay[ (int) random( svgsDay.length ) ] );
		} else {
			shapes[ i ] = new FormShape( svgsNight[ (int) random( svgsNight.length ) ] );
		}
	}

	// fill up the rest
	for ( int i = 6; i < 10; i++ ) {
		shapes[ i ] = new FormShape( svgsSunny[ (int) random( svgsSunny.length ) ] );
	}
}

void redrawShapes () {
	// create lines
	for ( int i = 0; i < 10; i++ ) {
		if ( i < 9 ) {
			stroke(0);
			strokeWeight(5);
			drawDottedLine( shapes[ i ].x, shapes[ i ].y, shapes[ i + 1 ].x, shapes[ i + 1 ].y );
		}	
	}

	// redraw shapes
	for ( int i = 0; i < 10; i++ ) {
		shapes[ i ].drawShape();
	}
}

void drawDottedLine ( int x1, int y1, int x2, int y2 ) {
	for ( int i = 0; i < 15; i++ ) {
		float x = lerp( x1, x2, i / 15.0 );
		float y = lerp( y1, y2, i / 15.0 );
		point( x, y );
	}
}

class FormShape {
	public int formWidth;
	public int formHeight;
	public float rotation;
	public int x;
	public int y;
	public int svgIndex;
	public float rotationSin;
	public float rotationIntensity;

	public PShape formShape;

	FormShape ( String svgPath ) {
		formWidth = (int) random( 100, 200 );
		formHeight = formWidth;
		rotationSin = 0;
		rotationIntensity = random( -0.03, 0.03 );
		x = (int) random( formWidth / 2, width - formWidth / 2 );
		y = (int) random( formHeight / 2, height - formHeight / 2 );

		if ( weather.getWindTemperature() > 13 ) {
		    formShape = loadShape( svgPath );
		} else {
		    formShape = loadShape( svgPath );
		}
	}	

	void drawShape () {
		rotationSin += rotationIntensity;
		rotation = 0.2 * sin( rotationSin );

		pushMatrix();
		translate( x, y );
		rotate( rotation );
		translate( -formWidth / 2, -formHeight / 2 );
		shape( formShape, 0, 0, formWidth, formHeight );
		popMatrix();
	}
}
